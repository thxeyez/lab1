/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain;

/**
 *
 * @author Room107
 */
public enum TripType {
    ROUND_TRIP,
    ONE_WAY
}
