/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain;

/**
 *
 * @author Room107
 */
public enum TravelClass {
   BUSINESS_CLASS("BUSINESS"),FIRST_CLASS("FIRST CLASS"),ECONOMY_CLASS("ECONOMY");
    
   public String ClassName;
   
   TravelClass(String ClassName){
   this.ClassName= ClassName;
   }
}
